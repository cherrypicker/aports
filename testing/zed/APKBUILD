# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=zed
pkgver=0.137.6
_pkgver=${pkgver/_/-}
pkgrel=3
pkgdesc="High-performance multiplayer code editor (experimental package!)"
url="https://zed.dev"
# TODO: Enable on more arches later.
arch="aarch64 x86_64"
license="GPL-3.0-only"
depends="
	nodejs
	npm
	"
makedepends="
	alsa-lib-dev
	cargo
	cargo-auditable
	clang-dev
	curl-dev
	fontconfig-dev
	libgit2-dev
	libxcb-dev
	libxkbcommon-dev
	mimalloc2-dev
	openssl-dev
	protoc
	sqlite-dev
	vulkan-loader
	wayland-dev
	zstd-dev
	"
source="https://github.com/zed-industries/zed/archive/v$_pkgver/zed-$_pkgver.tar.gz
	system-curl.patch
	system-openssl.patch
	system-sqlite.patch
	dont-download-node.patch
	dont-download-supermaven.patch
	disable-autoupdate.patch
	zed-libexec.patch
	initial_user_settings.patch
	cargo.lock.patch
	"
builddir="$srcdir/$pkgname-$_pkgver"
options="!check"  # FIXME: building fails on out of memory

# TODO:
# - unbundle fonts
# - unbundle other assets (?)
# - unbundle tree-sitter parsers
# - remove Supermaven integration (not usable on Alpine)?

unset CARGO_PROFILE_RELEASE_PANIC  # is this needed?
export CARGO_PROFILE_RELEASE_STRIP="symbols"

export RELEASE_VERSION="$_pkgver"

prepare() {
	default_prepare

	# Rust target triple.
	local target="$(rustc -vV | sed -n 's/host: //p')"

	# Build against system-provided libs.
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
		mimalloc = { rustc-link-lib = ["mimalloc"] }
		zstd = { rustc-link-lib = ["zstd"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release -p zed
	cargo auditable build --frozen --release -p cli
}

check() {
	cargo test --workspace --frozen
}

package() {
	install -D -m755 target/release/Zed -t "$pkgdir"/usr/libexec/
	install -D -m755 target/release/cli "$pkgdir"/usr/bin/zed

	install -D -m644 crates/zed/resources/zed.desktop -t "$pkgdir"/usr/share/applications/
	install -D -m644 crates/zed/resources/app-icon.png "$pkgdir"/usr/share/icons/hicolor/512x512/apps/zed.png
	install -D -m644 crates/zed/resources/app-icon@2x.png "$pkgdir"/usr/share/icons/hicolor/1024x1024/apps/zed.png
}

sha512sums="
597843e9d52be16e10da4ed20e142d2c43b30052cce150d08932cff25462ee84b2625fad6dd2420221bd48b587f68c0c8061053ee8cfbbd68421a1221759b879  zed-0.137.6.tar.gz
31f53310d35c2e9112460007c899fedda66d24561d7348f39ca8809423f4c5d6c22721c0d3e155d859c406733fb137d7be7015770e242b64b963cb2fe1b8f430  system-curl.patch
e05bd7921f5b611d7ed7857b4a613b2b01b152794d5a7e6bcc4c5767e9bd10937be29ace0a4af154104e631a8665f10433828c7744b29a7bffc825adaac30da4  system-openssl.patch
54a17784e428efc1564298f9e2a79e6c78386a4af47ea8c5d6c1df3543305258937b654d40841e281e9abadeb0f11ab6d5b1cc463d0d5770eceeace0819f833a  system-sqlite.patch
a54c76d4ce1206d9a06052d6356ba37c67e8e306699eec91a20b390de9ce05a48f127b15d2a6bf17d8c0c1fe22c0eae7b3a98de3c55620885013c2ea111e13d1  dont-download-node.patch
c6aab3cb834a3e345324632499599f15dd2856f3d5fbffcc280fd31cbcb76c5f5fb0efe74cb55e53568058100b287101a558e53f88b7b81beb1f8136e61731c5  dont-download-supermaven.patch
6b95f9fbeb8afe97ed44c4703cab86e206cf11d9dc85ea3cb250bb7df10f8f6cf0f0dd151e10da863262c148472f3fc57cb54ddc41953ac2655ee3a7ff8f79e8  disable-autoupdate.patch
353b3317396fbcdce12bca59eb7af16c2618c0965388b014f4940efbfdbb2d4dfe000cfb27bbce0f5c5e6e81182381ef85b10ace7b0f0605e0af4b63b68d7470  zed-libexec.patch
6adb5ac9ee34ab68771608d632e440125cd97c13c2ef67c00868d7366916dc5c3941523f7df3b490e6dd94cd47c742dc0df3cb241442fb2753bb28980f85dbb8  initial_user_settings.patch
69586a09cbb9ba560414ea020260e535ef25ff8d194266892d4f27c3744f00e8704a4947c898631503d0133886b31d30e06dfc12b78c4147340baf7b88d7ba04  cargo.lock.patch
"
